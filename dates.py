"""
srcpy.dates
"""

import pendulum as pm

class Dates:

    def today(self):
        return Conv(pm.now().naive())

    def from_st(self, st):
        return Conv(pm.from_format(st, Conv.pat))

    def from_dt(self, dt):
        return Conv(dt)


class Conv:
    pat = "YYYY-MM-DD"
    
    def __init__(self, dt):
        self.dt = dt
        self.st = dt.format(self.pat)

    def next(self):
        return Conv(self.dt.add(days=1))
    
    def sub(self, days=0):
        return Conv(self.dt.subtract(days=days))

    def add(self, days=0):
        return Conv(self.dt.add(days=days))

    def is_future(self):
        return self.dt > pm.now()
