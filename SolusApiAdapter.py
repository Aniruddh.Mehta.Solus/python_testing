import datetime
import json
import pandas as pd
import os
import sys
import time
import smtplib
import ssl


session = ""
user_data_list = []
total_customer_count = 0
log_path = "/client/logs/adapterlogs/"
home_path = os.path.expanduser('~')


adapter_start_date_time =  datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

def writeLog(file_name, msg):
    """
    This Function is for writing logs
    :param file_name:
    :param msg:
    :return:
    """
    file = open(file_name, 'a+')
    file.write(msg)
    file.write("    time: " + str(datetime.datetime.now()))
    file.write("\n")
    file.close()


payload_data_file_path = sys.argv[1]
adapter_settings = ""

try:
    adapter_settings_file = home_path+"/client/config/"+"adapter_settings.json"
    adapter_setting_json = open(adapter_settings_file, "r")
    adapter_settings = adapter_setting_json.read()
    adapter_settings = json.loads(adapter_settings)
except FileNotFoundError:
    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
    msg = "adapter_setting.json file is not available at path {0}".format(payload_data_file_path)
    writeLog(program_progress_log_file, msg)
    sys.exit("Please check the error logs")
except OSError:
    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
    msg = "Not able to open the file adapter_settings.json"
    writeLog(program_progress_log_file, msg)
    sys.exit("Please check the error logs")
except Exception as e:
    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
    msg = repr(e)
    writeLog(program_progress_log_file, msg)
    sys.exit("Please check the error logs")
request_json_file = open(home_path+"/client/config/"+"adapter_request.json","r")
request_json = request_json_file.read()
request_json = json.loads(request_json,encoding="utf-8")
if adapter_settings.get("isAuthReq") == "true":
    if request_json.get("Auth_API").get("api") == "" or request_json.get("Auth_API").get("methodtype") == "" or \
            request_json.get("Auth_API").get("params") == "" or request_json.get("Auth_API").get("headers") == "":
        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
        msg = "In credentials.json isAuthReq is true but in adapter_request.json file Auth_API details is not" \
              " configured correctly. Please check!"
        writeLog(program_progress_log_file, msg)
        sys.exit("Please Check The Execution Status Logs")

max_chunks = adapter_settings.get("max_limit")


def findkeyAuthResponse(data, response_type):
    global session
    log = {}
    try:
        if response_type == "json":
            if isinstance(data, dict):
                for key, value in data.items():
                    if key == request_json.get("Auth_API_Response_Token_Key_Value_Mapping").get("key"):
                        if request_json.get("Auth_API_Response_Token_Key_Value_Mapping").get("value") == "":
                            session = value
                            return {"status": True, "session": session, "api_response": data,
                                    "status_code": "200"}
                        else:
                            return {"status": False, "api_response": data, "status_code": "200"}
                    if isinstance(value, dict):
                        return findkeyAuthResponse(value, response_type)
            else:
                error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                log["msg"] = "Auth_API_Response_Type in adapter_request.json file is not configured correctly." \
                             "json is defined but Auth api response is not " \
                             "in json format."
                writeLog(error_log_file_name, str(log))
                sys.exit("Please check the error logs")
        else:
            if data.find(request_json.get("Auth_API_Response_Token_Key_Value_Mapping")) != -1:
                return {"status": True, "api_response": data, "status_code": "200"}
            else:
                error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                log["msg"] = data
                writeLog(error_log_file_name, str(log))
                sys.exit("Please check the error logs")
    except:
        error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
        log["msg"] = "Auth_API_Response_Type in adapter_request.json file is not configured correctly.Please check"
        writeLog(error_log_file_name, str(log))
        sys.exit("Please check the error logs")


def findkeyUploadResponse(data, response_type):
    log = {}
    try:
        if response_type == "json":
            if isinstance(data,str):
                data = json.loads(data)
            if isinstance(data, dict):
                for key, value in data.items():
                    if key == request_json.get("Upload_API_Response_Success_Key_Value_Mapping").get("key"):
                        if request_json.get("Upload_API_Response_Success_Key_Value_Mapping").get("value")=="":
                            return {"status": True, "msg": "data uploaded successfully", "api_response": data,
                                    "status_code": "200"}
                        elif request_json.get("Upload_API_Response_Success_Key_Value_Mapping").get("value") in [str(value), str(json.dumps(value)),value]:
                            return {"status": True, "msg": "data uploaded successfully", "api_response": data,
                                    "status_code": "200"}
                        else:
                            return {"status": True, "msg": "Not able to upload the data", "api_response": data,
                                        "status_code": "400"}
                    if isinstance(value, dict):
                        return findkeyUploadResponse(value, response_type)
                    if isinstance(value,list):
                        for obj in value:
                            status = findkeyUploadResponse(obj,"json")
                            if status.get("status"):
                                return status
            else:
                error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                log["msg"] = "Upload_API_Response_Type in adapter_request.json file is not configured correctly." \
                             "json is defined but Upload api response is not " \
                             "in json format."
                writeLog(error_log_file_name, str(log))
                sys.exit("Please check the error logs")
        else:
            if data.find(request_json.get("Upload_API_Response_Success_Key_Value_Mapping").get("key")) != -1:
                return {"status": True, "msg": "data uploaded successfully", "api_response": data, "status_code": "200"}
            else:
                return {"status": True, "msg": "Not able to upload the data", "api_response": data,
                                        "status_code": "400"}
                # error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                # log["msg"] = data
                # writeLog(error_log_file_name, str(log))
                # sys.exit("Please check the error logs")
    except:
        error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
        log["msg"] = "Upload_API_Response_Type in adapter_request.json file is not configured correctly.Please check"
        writeLog(error_log_file_name, str(log))
        sys.exit("Please check the error logs")


def get_api(url, params, headers, is_auth_api):
    """
    In this function we are making get api call.
    :param url: api_url(end point of api)
    :param headers: headers signature like content type authorization etc.
    :param params: data you need to pass in api request
    :return:
    """
    response = requests.get(url, data=json.dumps(params), headers=headers)
    if response.status_code in [200,201,202]:
        try:
            response_json = response.json()
        except:
            response_json = response.text
        if is_auth_api:
            find_key_status = findkeyAuthResponse(response_json, request_json.get("Auth_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.get(url, params=params, headers=headers)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    find_key_status = findkeyAuthResponse(response2_json,
                                                          request_json.get("Auth_API_Response_Type"))
                    return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
        else:
            find_key_status = findkeyUploadResponse(response_json, request_json.get("Upload_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.get(url, params=params, headers=headers)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    find_key_status = findkeyUploadResponse(response2_json,
                                                            request_json.get("Upload_API_Response_Type"))
                    return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
    else:
        response2 = requests.get(url, params=params, headers=headers)
        if response2.status_code in [200,201,202]:
            try:
                response2_json = response2.json()
            except:
                response2_json = response2.text
            if is_auth_api:
                find_key_status = findkeyAuthResponse(response2_json, request_json.get("Auth_API_Response_Type"))
                return find_key_status
            else:
                find_key_status = findkeyUploadResponse(response2_json, request_json.get("Upload_API_Response_Type"))
                return find_key_status
        return {"status": False, "status_code": str(response2.status_code), "api_response": response2}


def post_api_form_data(url, data, headers,files ,is_auth_api):
    """
    In this function we are making post api call.
    :param url: api_url(end point of api)
    :param payload_data: data you need to pass in api request
    :param headers: headers signature like content type authorization etc.
    :return:
    """
    response = requests.post(url, data=data, headers=headers, files=files)
    if response.status_code in [200,201,202]:
        try:
            response_json = response.json()
        except:
            response_json = response.text
        if is_auth_api:
            find_key_status = findkeyAuthResponse(response_json, request_json.get("Auth_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.post(url, json=data, headers=headers)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    if is_auth_api:
                        find_key_status = findkeyAuthResponse(response2_json,
                                                              request_json.get("Auth_API_Response_Type"))
                        return find_key_status
                    else:
                        find_key_status = findkeyUploadResponse(response2_json,
                                                                request_json.get("Upload_API_Response_Type"))
                        return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
        else:
            find_key_status = findkeyUploadResponse(response_json, request_json.get("Upload_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.post(url, json=data, headers=headers,files=files)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    if is_auth_api:
                        find_key_status = findkeyAuthResponse(response2_json,
                                                              request_json.get("Auth_API_Response_Type"))
                        return find_key_status
                    else:
                        find_key_status = findkeyUploadResponse(response2_json,
                                                                request_json.get("Upload_API_Response_Type"))
                        return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
    else:
        response2 = requests.post(url, json=data, headers=headers,files=files)
        if response2.status_code in [200,201,202]:
            try:
                response2_json = response2.json()
            except:
                response2_json = response2.text
            if is_auth_api:
                find_key_status = findkeyAuthResponse(response2_json, request_json.get("Auth_API_Response_Type"))
                return find_key_status
            else:
                find_key_status = findkeyUploadResponse(response2_json, request_json.get("Upload_API_Response_Type"))
                return find_key_status
        return {"status": False, "status_code": str(response2.status_code), "api_response": response2}


def post_api(url, data, headers, is_auth_api):
    """
    In this function we are making post api call.
    :param url: api_url(end point of api)
    :param payload_data: data you need to pass in api request
    :param headers: headers signature like content type authorization etc.
    :return:
    """
    try:
        response = requests.post(url, data=data, headers=headers)
        if (response.status_code in [200,201,202]):
            try:
                response_json = response.json()
            except:
                response_json = response.text
            if(response_json == ""):
                raise Exception
    except Exception:
        response = requests.post(url, json=data, headers=headers)
    if response.status_code in [200,201,202]:
        try:
            response_json = response.json()
        except:
            response_json = response.text
        if is_auth_api:
            find_key_status = findkeyAuthResponse(response_json, request_json.get("Auth_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.post(url, json=data, headers=headers)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    if is_auth_api:
                        find_key_status = findkeyAuthResponse(response2_json,
                                                              request_json.get("Auth_API_Response_Type"))
                        return find_key_status
                    else:
                        find_key_status = findkeyUploadResponse(response2_json,
                                                                request_json.get("Upload_API_Response_Type"))
                        return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
        else:
            find_key_status = findkeyUploadResponse(response_json, request_json.get("Upload_API_Response_Type"))
            if find_key_status.get("status"):
                return find_key_status
            else:
                response2 = requests.post(url, json=data, headers=headers)
                if response2.status_code in [200,201,202]:
                    try:
                        response2_json = response2.json()
                    except:
                        response2_json = response2.text
                    if is_auth_api:
                        find_key_status = findkeyAuthResponse(response2_json,
                                                              request_json.get("Auth_API_Response_Type"))
                        return find_key_status
                    else:
                        find_key_status = findkeyUploadResponse(response2_json,
                                                                request_json.get("Upload_API_Response_Type"))
                        return find_key_status
                return {"status": False, "status_code": str(response2.status_code), "api_response": response2}
    else:
        response2 = requests.post(url, json=data, headers=headers)
        if response2.status_code in [200,201,202]:
            try:
                response2_json = response2.json()
            except:
                response2_json = response2.text
            if is_auth_api:
                find_key_status = findkeyAuthResponse(response2_json, request_json.get("Auth_API_Response_Type"))
                return find_key_status
            else:
                find_key_status = findkeyUploadResponse(response2_json, request_json.get("Upload_API_Response_Type"))
                return find_key_status
        return {"status": False, "status_code": str(response2.status_code), "api_response": response2}





def send_mail(emailtype):
    """
    this fuction is to send the mails.
    :param type:
    :return:
    """
    msg = MIMEMultipart()
    msg["Subject"] = adapter_settings.get("email_configuration").get("subject")
    msg["From"] = adapter_settings.get("email_configuration").get("sender")
    msg["to"] = adapter_settings.get("email_configuration").get("receiver")
    if emailtype == "initiated":
        body = adapter_settings.get("email_configuration").get("email_initiated_content") + "\n\n" + "Thanks, IT"
    if emailtype == "completion":
        body = adapter_settings.get("email_configuration").get("email_completion_content") + "\n\n" + "Thanks, IT"
    msg.attach(MIMEText(body, 'plain'))
    context = ssl.create_default_context()
    with smtplib.SMTP(adapter_settings.get("email_configuration").get("domain"), adapter_settings.get("port")) as smtp:
        smtp.starttls(context=context)
        smtp.login(adapter_settings.get("email_configuration").get("sender"), adapter_settings.get("password"))
        smtp.send_message(msg)


def generate_token(Auth_API):
    """ This Function is used for generating token which is required to call before bknl push coupon api
        if you also need generate some kind of session token then you can modify this function as of your requirement.
    """
    # header_mapping = Auth_API.get("headers")
    # headers2 = {}
    # if header_mapping.get("content_type_key") != "" and header_mapping.get("content_type_value") != "":
    #     headers2 = {header_mapping.get("content_type_key"): header_mapping.get("content_type_value")}
    # if header_mapping.get("auth_key") != "" and header_mapping.get("auth_value") != "":
    #     headers2[header_mapping.get("auth_key")] = header_mapping.get("auth_value")
    try:
        if Auth_API.get("methodtype") == "get":
            # response = get_api(Auth_API.get("api"), params=json.dumps(Auth_API.get("params")),
            #                    headers=headers2, is_auth_api=True)
            response = get_api(Auth_API.get("api"), params=json.dumps(Auth_API.get("params")),
                               headers=Auth_API.get("headers"), is_auth_api=True)
            if response.get("status"):
                response = findkeyAuthResponse(response, request_json.get("Auth_API_Response_Type"))
                return response
            else:
                return {"status": False, "msg": "Not able to generate the access token",
                        "status_code": str(response.status_code), "api_response_code": response.status_code}

        if Auth_API.get("methodtype") == "post":
            # response = post_api(Auth_API.get("api"), data=Auth_API.get("params"),
            #                     headers=headers2,
            #                     is_auth_api=True)
            response = post_api(Auth_API.get("api"), data=Auth_API.get("params"),
                                headers=Auth_API.get("headers"),
                                is_auth_api=True)
            if response.get('status'):
                response = findkeyAuthResponse(response, request_json.get("Auth_API_Response_Type"))
                return response
            else:
                return {"status": False, "msg": "Not able to generate the access token",
                        "api_response_code": response.status_code, "status_code": str(response.status_code)}

    except Exception as e:
        return {"status":False, "msg":repr(e), "status_code": "500", "api_response_code": repr(e)}


def run(file_path):
    """
    This is main function to read data from payload files and upload the data using api.
    :param :
    :return:
    """
    global session
    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
    msg = "Execution Started at:"
    writeLog(program_progress_log_file, msg)
    if adapter_settings.get("is_support_emails") == "true":
        send_mail("initiated")
    if adapter_settings.get("isAuthReq") == "true":
        if request_json.get("Auth_API").get("api") == "" or request_json.get("Auth_API").get("methodtype") == "" or \
                request_json.get("Auth_API").get("params") == "" or request_json.get("Auth_API").get("headers") == "":
            program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
            msg = "adapter_request.json file is not configure correctly for Auth_API."
            writeLog(program_progress_log_file, msg)
            sys.exit("Please check the error logs")

        session_response = generate_token(request_json.get("Auth_API"))
        if session_response.get("status"):
            session = session_response.get("session")
            generate_toke_log_file_name = home_path+log_path+"API_GENERATE_session_LOG_" + adapter_start_date_time + ".txt"
            log2 = {"url": request_json.get("Auth_API").get("api"), "msg": str(session_response)}
            writeLog(generate_toke_log_file_name, str(log2))
        else:
            generate_toke_log_file_name = home_path+log_path+"API_GENERATE_session_LOG_" + adapter_start_date_time + ".txt"
            log2 = {"url": request_json.get("Auth_API").get("api"), "msg": str(session_response)}
            writeLog(generate_toke_log_file_name, str(log2))
            sys.exit("Please check the error logs")
    header_mapping = request_json.get("Upload_API_Header_Key_Mapping").get("headers")
    headers2 = {}
    if header_mapping.get("content_type_key") != "" and header_mapping.get("content_type_value") != "":
        headers2 = {header_mapping.get("content_type_key"): header_mapping.get("content_type_value")}
    if header_mapping.get("auth_key") != "":
        headers2[header_mapping.get("auth_key")] = header_mapping.get("auth_value")
    if header_mapping.get("auth_key") in headers2:
        if headers2.get(header_mapping.get("auth_key")) == "":
            headers2[header_mapping.get("auth_key")] = session
        else:
            headers2[header_mapping.get("auth_key")] = header_mapping.get("auth_value") + "" + session

    if request_json.get("Upload_API").get("api") == "" or request_json.get("Upload_API").get("methodtype") == "" or \
            request_json.get("Upload_API").get("headers") == "":
        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
        msg = "adapter_request.json file is not configure correctly for Upload_API."
        writeLog(program_progress_log_file, msg)
        sys.exit("Please check the error logs")
    if request_json.get("Upload_API").get("headers").get("Content-Type") == "":
        all_files = os.listdir(file_path)
        csv_files = list(filter(lambda x:x[-4:] == ".csv",all_files))
        for file_name in csv_files:
            try:
                # payload_data = request_json.get("Upload_API").get("params")
                payload_data = {'notification':str(request_json.get("Upload_API").get("params").get("notification")).replace("'",'"'),
                                'config':str(request_json.get("Upload_API").get("params").get("config")).replace("'",'"')}
                # payload_data = str(payload_data.get("notification")).replace("'",'"')
                # payload_data = {'notification': 
                #                '{"type":"whatsapp","sender":"919538197599","templateId":"pe_new"}',
                #                'config': '{"customPayload":{"name":"name","city":"city"}}'}

                data = pd.read_csv(file_path + file_name, sep=request_json.get("Upload_API").get("csv_file_separator"), dtype=str, na_filter=False)
                datafile = [("userDetails",(file_name,open(file_path+file_name,"rb"),'text/csv'))]
                api = request_json.get("Upload_API").get("api")
                if(request_json.get("Upload_API").get("is_req_to_add_dynamic_value_to_api"))=="true":
                    api = request_json.get("Upload_API").get("api") + data.get(request_json.get("Upload_API").get("dynamic_csv_header_name"))[0]
                if request_json.get("Upload_API").get("methodtype") == "post":
                    response = post_api_form_data(api, payload_data, headers2,datafile, False)
                else:
                    response = get_api(request_json.get("Upload_API").get("api"), payload_data, headers2, False)
                if response.get("status_code") == '200':
                    log = {"url": request_json.get("Upload_API").get("api")}
                    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                    log["msg"] = "customer's data has been uploaded."
                    writeLog(program_progress_log_file,str(log))
                    success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                    log["msg"] = str(payload_data)
                    writeLog(success_log_with_payload_file_name, str(log))
                    success_log_file_name = home_path+log_path+"API_SUCCESS_LOG_" + adapter_start_date_time + ".txt"
                    log["msg"] = str(response)
                    writeLog(success_log_file_name, str(log))
                    continue
                elif response.get("status_code") == "400":
                    error_log_file_name = home_path+log_path+"DATA_IGNORE_API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                    log["msg"] = str(response)
                    writeLog(error_log_file_name, str(log))
                    continue
            except FileNotFoundError:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = "{0} is not avalable at path {1}".format(file_name, file_path)
                writeLog(program_progress_log_file, msg)
                continue
            except OSError:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = "Not able to open the file {0}.".format(file_name)
                writeLog(program_progress_log_file, msg)
            except Exception as e:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = repr(e)
                writeLog(program_progress_log_file, msg)
                sys.exit("Please check EXECUTION_STATUS_LOG file for the error logs")
    else:
        all_files = os.listdir(file_path)
        json_files = list(filter(lambda x: x[-5:] == '.json', all_files))
        for file_name in json_files:
            user_data_list = []
            try:
                payload_file_data = open(file_path + file_name, "r")
                user_data_list = json.loads(payload_file_data.read())
                # user_data_list = json.loads(payload_file_data)
                # if adapter_settings.get("batch_payload_attribute") != "":
                #     user_data_list = payload_file_data.get(adapter_settings.get("batch_payload_attribute"))
                # else:
                #     user_data_list.append(payload_file_data)

                if isinstance(user_data_list, list):
                    if len(user_data_list) < 1:
                        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                        msg = "payload.json file is Empty."
                        writeLog(program_progress_log_file, msg)
                        sys.exit("Please check execution status log error logs")
                i = 0
                while i < len(user_data_list):
                    payload_data = {}
                    log = {"url": request_json.get("Upload_API").get("api")}
                    # for key, value in payload_file_data.items():
                    #     if key != adapter_settings.get("batch_payload_attribute"):
                    #         payload_data[key] = value
                    # if adapter_settings.get("batch_payload_attribute") != "":
                    payload_data = user_data_list[i:i + max_chunks]
                    if(max_chunks) == 1:
                        payload_data = payload_data[0]
                    if request_json.get("Upload_API").get("methodtype") == "post":
                        response = post_api(request_json.get("Upload_API").get("api"), payload_data, headers2, False)
                    else:
                        response = get_api(request_json.get("Upload_API").get("api"), payload_data, headers2, False)
                    if response.get("status_code") == '200':
                        i = i + max_chunks
                        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                        msg = "customer's data has been uploaded."
                        writeLog(program_progress_log_file, msg)
                        success_log_with_payload_file_name = home_path + 
                                                             log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + 
                                                             adapter_start_date_time + ".txt"
                        log["msg"] = str(payload_data)
                        writeLog(success_log_with_payload_file_name, str(log))
                        success_log_file_name = home_path+log_path+"API_SUCCESS_LOG_" + adapter_start_date_time + ".txt"
                        log["msg"] = str(response)
                        writeLog(success_log_file_name, str(log))
                        continue
                    elif response.get("status_code") == "400":
                        error_log_file_name = home_path+log_path+"DATA_IGNORE_API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                        log["msg"] = str(response)
                        writeLog(error_log_file_name, str(log))
                        error_log_file_name_with_payload = home_path+log_path+"DATA_IGNORE_API_ERROR_LOG_WITH_PAYLOAD"+ adapter_start_date_time + ".txt"
                        log["msg"] = str(payload_data) + str(response)
                        writeLog(error_log_file_name_with_payload, str(log))
                        i = i + max_chunks
                        continue
                        # sys.exit("Please check the error logs")
                    elif response.get("status_code") == "401":
                        error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                        log["msg"] = str(response)
                        writeLog(error_log_file_name, str(log))
                        try:
                            if adapter_settings.get("isAuthReq") == "true":
                                session_response = generate_token(request_json.get("Auth_API"))
                                if session_response.get("status"):
                                    session = session_response.get("session")
                                    generate_toke_log_file_name = home_path+log_path+"API_GENERATE_session_LOG_" + adapter_start_date_time + ".txt"
                                    log2 = {}
                                    log2["url"] = request_json.get("Auth_API").get("api")
                                    log2["msg"] = str(session_response)
                                    writeLog(generate_toke_log_file_name, str(log2))
                                else:
                                    generate_toke_log_file_name = home_path+log_path+"API_GENERATE_session_LOG_" + adapter_start_date_time + ".txt"
                                    log2 = {}
                                    log2["url"] = request_json.get("Auth_API").get("api")
                                    log2["msg"] = str(session_response)
                                    writeLog(generate_toke_log_file_name, str(log2))
                                    sys.exit("Please check the error logs")
                                header_mapping = request_json.get("Upload_API_Header_Key_Mapping").get("headers")
                                headers2 = {
                                    header_mapping.get("content_type_key"): header_mapping.get("content_type_value"),
                                    header_mapping.get("auth_key"): header_mapping.get("auth_value")}
                                if headers2.get(header_mapping.get("auth_key")) == "":
                                    headers2[header_mapping.get("auth_key")] = headers2[
                                                                                header_mapping.get(
                                                                                    "auth_key")] + session
                                else:
                                    headers2[header_mapping.get("auth_key")] = headers2[header_mapping.get(
                                        "auth_key")] + " " + session
                            response = post_api(request_json.get("Upload_API").get("api"), payload_data, headers2, False)
                            if response.get("status_code") == "200":
                                if findkeyUploadResponse(response, request_json.get("Upload_API").get(
                                        "Upload_API_Response_Type")).get("status"):
                                    i = i + max_chunks
                                    success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                                    log["msg"] = str(payload_data)
                                    writeLog(success_log_with_payload_file_name, str(log))
                                    success_log_file_name = home_path+log_path+"API_SUCCESS_LOG_" + adapter_start_date_time + ".txt"
                                    log["msg"] = str(response)
                                    writeLog(success_log_file_name, str(log))
                                    program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                                    msg = "customer's data has been uploaded."
                                    writeLog(program_progress_log_file, msg)
                                    success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                                    log["msg"] = str(payload_data)
                                    writeLog(success_log_with_payload_file_name, str(log))
                                    continue
                            if response.get("status_code") == "504":
                                time.sleep(60)
                                response = post_api(request_json.get("Upload_API").get("api"), payload_data, headers2,
                                                    False)
                                if response.get("status_code") == "200":
                                    if findkeyUploadResponse(response, request_json.get("Upload_API").get(
                                            "Upload_API_Response_Type")).get("status"):
                                        i = i + max_chunks
                                        success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                                        log["msg"] = str(payload_data)
                                        writeLog(success_log_with_payload_file_name, str(log))
                                        success_log_file_name = home_path+log_path+"API_SUCCESS_LOG_" + adapter_start_date_time + ".txt"
                                        log["msg"] = str(response)
                                        writeLog(success_log_file_name, str(msg))
                                        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                                        msg = "customer's data has been uploaded."
                                        writeLog(program_progress_log_file, msg)
                                        success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                                        log["msg"] = str(payload_data)
                                        writeLog(success_log_with_payload_file_name, str(log))
                                        continue
                                if response.get("status_code") != "200":
                                    error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                                    log["msg"] = str(response)
                                    writeLog(error_log_file_name, str(log))
                                    sys.exit("Please check the error logs")
                        except Exception as e:
                            error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                            log["msg"] = str(e)
                            writeLog(error_log_file_name, str(log))
                            sys.exit("Please check the error logs")
                    elif response.get("status_code") == "504":
                        time.sleep(60)
                        response = post_api(request_json.get("Upload_API").get("api"), payload_data, headers2, False)
                        if response.get("status_code") == "200":
                            i = i + max_chunks
                            success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                            log["msg"] = str(payload_data)
                            writeLog(success_log_with_payload_file_name, str(log))
                            success_log_file_name = home_path+log_path+"API_SUCCESS_LOG_" + adapter_start_date_time + ".txt"
                            log["msg"] = str(response)
                            writeLog(success_log_file_name, str(log))
                            program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                            msg = "customer's data has been uploaded."
                            writeLog(program_progress_log_file, msg)
                            success_log_with_payload_file_name = home_path+log_path+"API_SUCCESS_LOG_WITH_PAYLOAD" + adapter_start_date_time + ".txt"
                            log["msg"] = str(payload_data)
                            writeLog(success_log_with_payload_file_name, str(log))
                            continue
                        else:
                            error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                            log["msg"] = str(response)
                            writeLog(error_log_file_name, str(log))
                            sys.exit("Please check the error logs")
                    else:
                        error_log_file_name = home_path+log_path+"API_ERROR_LOG_" + adapter_start_date_time + ".txt"
                        log["msg"] = str(response)
                        writeLog(error_log_file_name, str(log))
                        sys.exit("Please check the error logs")
            except FileNotFoundError:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = "{0} is not avalable at path {0}".format(file_name, file_path)
                writeLog(program_progress_log_file, msg)
                continue
            except OSError:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = "Not able to open the file {0}.".format(file_name)
                writeLog(program_progress_log_file, msg)
            except Exception as e:
                program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
                msg = repr(e)
                writeLog(program_progress_log_file, msg)
                sys.exit("Please check EXECUTION_STATUS_LOG file for the error logs")

        program_progress_log_file = home_path+log_path+"EXECUTION_STATUS_LOG_" + adapter_start_date_time + ".txt"
        msg = "Execution Completed at:"
        writeLog(program_progress_log_file, msg)
        if adapter_settings.get("is_support_emails") == "true":
            send_mail("completion")


run(payload_data_file_path)
