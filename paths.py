"""
srcpy.path
"""

import os
import srcpy
import shutil
import pathlib as pb
import pandas as pd

from ..ops.data import db


class Paths:
    db = db.DB()
    client_data_root = pb.Path.home() / 'client' / 'mcore'
    client_bin_root = pb.Path.home() / 'mysolus' / 'mcore'
    repo_root = pb.Path(srcpy.__path__[0]).parent

    client_configs = client_bin_root / 'config'
    client_default_config = client_configs / 'mCoreDefaultConfig.csv'
    client_user_config = client_configs / 'mCoreUserConfig_mFactory.cfg'
    client_mcfg7 = client_configs / 'NextTrxn7.toml'
    client_mcfg15 = client_configs / 'NextTrxn15.toml'
    client_mcfg30 = client_configs / 'NextTrxn30.toml'

    # lconfigs = [
    #         default_config,
    #         user_config,
    #         mcfg7,
    #         mcfg15,
    #         mcfg30,
    #         ]

    data_default_config = repo_root / 'data' / 'default_config.csv'
    # default_check = repo_root / 'data' / 'default_check.csv'
    data_default_master = repo_root / 'data' / 'default_master.csv'
    repo_default_config = repo_root / 'config' / 'mCoreDefaultConfig.csv'
    repo_user_config = repo_root / 'config' / 'mCoreUserConfig_mFactory.cfg'
    repo_mcfg7 = repo_root / 'config' / 'NextTrxn7.toml'
    repo_mcfg15 = repo_root / 'config' / 'NextTrxn15.toml'
    repo_mcfg30 = repo_root / 'config' / 'NextTrxn30.toml'

    repo_library = repo_root / 'library'
    repo_featurizer = repo_root / 'library' / 'DailyFeaturizer.R'
    repo_mcore = repo_root / 'library' / 'mCore.R'
    repo_attach = repo_root / 'library' / 'AttachResponseAndFilter.R'
    repo_ftrchurn = repo_root / 'library' / 'LookAheadFTRChurnPredict.R'

    def __init__(self, model_name):
        self.client_data_root.mkdir(parents=True, exist_ok=True)
        self.model_name = model_name
        self.model_path = self.client_data_root / model_name

        self.x = self.model_path / 'x'
        self.xy = self.model_path / 'xy'
        self.model = self.model_path / 'model'
        self.scores = self.model_path / 'scores'
        self.eval = self.model_path / 'eval'
        self.gain = self.model_path / 'eval_GainChart.csv'

    def make_new_empty(self):
        self._make_new_empty(self.x)
        self._make_new_empty(self.xy)
        self._make_new_empty(self.model)
        self._make_new_empty(self.eval)
        self._make_new_empty(self.scores)

    # def read_score_file(self, model_date, score_date):
        # score_file = self.scores / f'scores+{model_date}+{score_date}.csv'
        # return pd.read_csv(score_file)

    def read_score_file(self):
        score_file = self.scores / 'scores.csv'
        return pd.read_csv(score_file)

    def read_ftr_score_file(self):
        file = self.scores / 'FTRScores.csv'
        if file.exists():
            df = pd.read_csv(file)
            df = df.rename(columns={'FTRScore': 'Score'})
            return df


    def read_churn_score_file(self):
        file = self.scores / 'ChurnScores.csv'
        if file.exists():
            df = pd.read_csv(file)
            df = df.rename(columns={'ChurnScore': 'Score'})
            return df

    def _make_new_empty(self, path):
        shutil.rmtree(path, ignore_errors=True) 
        path.mkdir(parents=True, exist_ok=True)

    def model_config_file(self):
        return self.client_configs / f"{self.model_name}.toml"


    def read_latest_model(self):
        return max(list(self.model.glob('*')), key=os.path.getctime)

    def list_of_input(self, date, relative=None):
        if relative is None:
            return ';'.join(str(x) for x in self.xy.glob(f'ModelData*{date}*'))
        else:
            return ';'.join(str(x.relative_to(relative)) for x in self.xy.glob(f'ModelData*{date}*'))

    def _x_to_xy(self, df_x, date_start, date_end):
        txn_cust = self.db.read_txn_cust(date_start.next().st, date_end.st)
        txn_cust['Response'] = 1
        merged = df_x.merge(txn_cust, on='Customer_Id', how='left') #type:ignore
        merged.Response.fillna(0, inplace=True)
        df_xy = merged.iloc[:, :]
        return df_xy

    def read_x_file(self, file):
        if file.suffix == '.feather':
            return pd.read_feather(file)
        elif file.suffix == '.csv':
            return pd.read_csv(file)

    def all_x_to_xy_to_file(self, date_start, date_end=None):
        for file in self.x.glob(f'CustomerVars*{date_start.st}*'):
            _, segment, _ = str(file).split('+')
            df_x_segment = self.read_x_file(file)
            df_xy_segment = df_x_segment
            if date_end is not None:
                df_xy_segment = self._x_to_xy(df_x_segment, date_start, date_end)
            self._segment_xy_to_file(df_xy_segment, segment, date_start.st)

    # def _combine_all_x(self, sdate):
    #     list_df = []
    #     for file in list(self.x.glob(f'CustomerVars*{sdate}*')):
    #         list_df.append(pd.read_feather(file))
    #     return pd.concat(list_df).reset_index(drop=True)

    def _segment_xy_to_file(self, df, segment, sdate):
        outputformat = os.environ.get("MCORE_FORMAT", "feather")
        if outputformat in ['csv', 'feather']:
            name = f'ModelData+{segment}+{self.model_name}+{sdate}.{outputformat}'
        if outputformat == 'feather':
            df.to_feather(self.xy / name) #type:ignore
        elif outputformat == 'csv':
            df.to_csv(self.xy / name, index=False) #type:ignore
        # name = f'ModelData+{segment}+{self.model_name}+{sdate}.feather'
        # df.to_feather(self.xy / name) #type:ignore

    # def _write_file(self, df, sdate):
    #     outputformat = os.environ.get("MCORE_FORMAT", "feather")
    #     if outputformat in ['csv', 'feather']:
    #         name = f'ModelData+ALL+{self.model_name}+{sdate}.{outputformat}'
    #     if outputformat == 'feather':
    #         df.to_feather(self.xy / name) #type:ignore
    #     elif outputformat == 'csv':
    #         df.to_csv(self.xy / name) #type:ignore

    # def combine_all_x_to_feather(self, sdate):
    #     self._write_file(self._combine_all_x(sdate), sdate)

    # def combine_all_x_and_y_to_feather(self, date_start, date_end):
    #     txn_cust = self.db.read_txn_cust(date_start.next().st, date_end.st)
    #     txn_cust['Response'] = 1
    #     merged = self._combine_all_x(date_start.st).merge(txn_cust, on='Customer_Id', how='left') #type:ignore
    #     merged.Response.fillna(0, inplace=True)
    #     model_data = merged.iloc[:, :]
    #     self._write_file(model_data, date_start.st)

    def _add_columns(self, df, date, type='oos'):
        df["TrainingEndDate"] = date

        if type == 'oos':
            test_type = "out-of-sample"
            chart_type = "Training"
        else:
            test_type = "out-of-time"
            chart_type = "Predicted"
        df["TestType"] = test_type
        df["ChartType"] = chart_type

        ser_base = df["Obs.0"] + df["Obs.1"]
        base = ser_base.iloc[0]
        df["Base"] = int(base/10)
        df["CumlBase"] = df.Base.cumsum()
        df["CumlConversionsPercent"] = df.CumulativeResponders / df.CumlBase
        return df

    def create_gain(self, date):
        df = pd.read_csv(self.gain)
        return self._add_columns(df, date)

    def create_final_gain(self, listdf):
        final_df = pd.concat(listdf, axis=0) # type: ignore
        final_df["ModelName"] = self.model_name
        final_df = final_df.rename(columns={
            'Responders': 'Conversions',
            'RespondersPercent': 'ConversionsPercent',
            'CumulativeRespondersPercent': 'Gain',
            'CumulativeResponders': 'CumlConversions',
            })
        return final_df



    # def create_gain_chart(self, date_back, date_fwd):
    #     df_fwd_fwd = pd.read_csv(self.eval / f'{self.model_name}+{date_fwd}+{date_fwd}_GainChart.csv')
    #     df_fwd_fwd = self._add_columns(df_fwd_fwd, date_fwd)

    #     df_fwd_back = pd.read_csv(self.eval / f'{self.model_name}+{date_back}+{date_fwd}_GainChart.csv')
    #     df_fwd_back = self._add_columns(df_fwd_back, date_back, type='oot')

    #     final_df = pd.concat([df_fwd_fwd, df_fwd_back], axis=0) # type: ignore
    #     final_df["ModelName"] = self.model_name
    #     final_df = final_df.rename(columns={
    #         'Responders': 'Conversions',
    #         'RespondersPercent': 'ConversionsPercent',
    #         'CumulativeRespondersPercent': 'Gain',
    #         'CumulativeResponders': 'CumlConversions',
    #         })
    #     return final_df
