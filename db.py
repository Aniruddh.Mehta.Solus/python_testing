"""
srcpy.ops.db
"""

import os
import click
import smokypuppy as sp
import sqlalchemy as sqlm
from ... import tools
from . import stats

class DB:
    engine = sp.engine_from_env()
    stats = stats.Stats()
    # stats.read_stats()
    limit = int(os.environ.get("MCORE_LIMIT", 1e16))
    mc_config = "CLM_MCore_Config"
    mc_check = "CLM_MCore_Check"
    mc_master = "CLM_MCore_Model_Master"
    mc_percent = "CLM_MCore_Customer_Percentile"
    mc_dashboard = "Dashboard_Insights_Propensity"
    cone = "V_CLM_Customer_One_View"
    cdmbh = "CDM_Bill_Header"
    cdmbd = "CDM_Bill_Details"

    def read_customers_of(self, table):
        return self.engine.read(f"select Customer_Id from {table}")
    
    def read_mc_master(self):
        return self.engine.read(f"select * from {self.mc_master}")
    
    def read_mc_check(self):
        df = self.engine.read(f"select * from {self.mc_check}")
        return df.iloc[:,1:]

    def exists_mc_dbconfig(self):
        return self.engine.con.has_table(self.mc_config)

    def exists_mc_check(self):
        return self.engine.con.has_table(self.mc_check)

    def exists_mc_percent(self):
        return self.engine.con.has_table(self.mc_percent)

    def exists_mc_master(self):
        return self.engine.con.has_table(self.mc_master)

    def exists_mc_dashboard(self):
        return self.engine.con.has_table(self.mc_dashboard)

    def read_mc_percent(self):
        return self.engine.read(f"select * from {self.mc_percent}")

    def read_mc_percent_customers(self):
        return self.read_customers_of(self.mc_percent)

    def write_mc_config(self, df, if_exists='replace'):
        self.engine.write_simple(df, self.mc_config, if_exists=if_exists)
        click.secho(f"written to {self.mc_config} table in DB")

    def write_mc_check(self, df, if_exists='replace'):
        self.engine.write_simple(df, self.mc_check, if_exists=if_exists, index=True, index_label='ID')
        click.secho(f"written to {self.mc_check} table in DB")

    def write_mc_master(self, df, if_exists='replace'):
        self.engine.write(df, self.mc_master, primary_key='ID', partition_by='ID', if_exists=if_exists)
        click.secho(f"written to {self.mc_master} table in DB")

    def write_mc_percent(self, df, if_exists='replace'):
        self.engine.write(df, self.mc_percent, if_exists=if_exists)
        click.secho(f"written to {self.mc_percent} table in DB")

    # def write_new_mc_percent(self, df, if_exists='replace'):
    #     self.engine.write_simple(df, self.mc_percent, if_exists=if_exists)
    #     click.secho(f"written to {self.mc_percent} table in DB")

    def read_mc_dashboard(self):
        return self.engine.read(f"select * from {self.mc_dashboard}")

    def write_mc_dashboard(self, df, if_exists='replace'):
        self.engine.write_simple(df, self.mc_dashboard, if_exists=if_exists, dtype={'Decile': sqlm.types.SMALLINT()})
        click.secho(f"written to {self.mc_dashboard} table in DB")

    def delete_dashboard_rows(self, model_name):
        self.engine.execute(f'delete from {self.mc_dashboard} where ModelName="{model_name}"')
        click.secho(f"deleted old data for {model_name} from {self.mc_dashboard}")

    # def delete_check_rows(self, check_name):
    #     self.engine.execute(f'delete from {self.mc_check} where Check="{check_name}"')
    #     click.secho(f"deleted old data for {check_name} from {self.mc_check}")

    def read_cone_customers(self):
        return self.read_customers_of(self.cone)

    def read_txn_cust(self, start, end):
        return self.engine.read(f"select distinct Customer_Id from CDM_Bill_Header where Bill_Date between '{start}' and '{end}'")

    def read_cust_with_seg(self):
        cust = self.read_cone_customers()
        seg = self.engine.read('select * from CLM_Segment where In_Use="Yes"')
        # for val in seg.CLMSegmentReplacedQuery.values:
        for _, row in seg.iterrows():
            df_one_seg = self.engine.read(f'select Customer_Id from V_CLM_Customer_One_View where {row.CLMSegmentReplacedQuery}')
            cust.loc[cust.Customer_Id.isin(df_one_seg.Customer_Id), 'CLMSegmentName'] = row.CLMSegmentName
        return cust

    # def read_cust_seg(self):
    #     return self.engine.read(f"select * from CDM_Customer_Segment")

    def read_check_fail_margin(self):
        return int(self.engine.read('select Value from CLM_MCore_Config where Config="check.fail.margin"').values[0][0])


    def read_ltd(self):
        pos = 1
        while True:
            ltd = self.engine.read(f"select Bill_Date from CDM_Bill_Details order by Bill_Date desc limit {pos-1}, 1").iloc[:, 0].astype(str).values[0]
            pos += 1
            ltd = tools.dates.from_st(ltd)
            if not ltd.is_future():
                break
        return ltd.st

    def read_active_period(self):
        return int(self.engine.read("select value from svoc_config where name='active_period'").values[0][0])

    def read_ftr_actual_count(self):
        return int(self.engine.read(
            f"select count(1) from V_CLM_Customer_One_View where Recency < {self.read_active_period()} and Lt_Num_of_Visits = 1").values[0][0])

    def read_churn_actual_count(self):
        return int(self.engine.read(
            f"select count(1) from V_CLM_Customer_One_View where Recency < {self.read_active_period()/2} and Lt_Num_of_Visits > 0").values[0][0])

