"""
srcpy.ffi
"""

import numpy as np
import subprocess
import smokypuppy as sp
from .. import tools

class Rinterface:

    def __init__(self, model_name):
        self.model_name = model_name
        self.paths = tools.Paths(model_name)

    def run_featurizer(self, obsenddate=None):
        #TODO: find a way to not run featurizer for a date already calculated
        sp.timer.start('run_featurizer')
        # outputformat = os.environ.get("MCORE_FORMAT", "feather")
        try:
            if obsenddate:
                subprocess.check_call([
                    'Rscript',
                    self.paths.repo_featurizer,
                    f"--modelconfigfile={self.paths.model_config_file()}",
                    f"--obsenddate={obsenddate}",
                    # f"--outputpath={self.paths.x}",
                    f"--librarypath={self.paths.repo_library}",
                    ])
            else:
                subprocess.check_call([
                    'Rscript',
                    self.paths.repo_featurizer,
                    f"--modelconfigfile={self.paths.model_config_file()}",
                    # f"--outputpath={self.paths.x}",
                    f"--librarypath={self.paths.repo_library}",
                    ])
        except Exception as e:
            print('Rscript halted with non-zero exit status')
            raise SystemExit(e)
        sp.timer.stop('run_featurizer')

    def run_attachresponseandfilter(self, startdate, enddate, mode):
        timer = 'run_attachresponseandfilter'
        sp.timer.start(timer)
        try:
            subprocess.check_call([
                'Rscript',
                self.paths.repo_attach,
                f"--librarypath={self.paths.repo_library}",
                f"--modelconfigfile={self.paths.model_config_file()}",
                f"--respstartdate={startdate}",
                f"--respenddate={enddate}",
                f"--mode={mode}",
                ])
        except Exception as e:
            print('Rscript halted with non-zero exit status')
            raise SystemExit(e)

        sp.timer.stop(timer)

    def run_mcore(self, mode):
        if mode == 'train':
            log_message = 'run_mcore_train'
        elif mode == 'score':
            log_message = 'run_mcore_score'

        sp.timer.start(log_message)
        try:
            subprocess.check_call([
                'Rscript', self.paths.repo_mcore, 
                f'--librarypath={self.paths.repo_library}',
                f'--mode={mode}',
                f"--modelconfigfile={self.paths.model_config_file()}",
            ])
        except Exception as e:
            print('Rscript halted with non-zero exit status')
            raise SystemExit(e)
        sp.timer.stop(log_message)

    def run_ftrchurn(self, scoretype, stridelength, maxperiod):
        if np.isnan(maxperiod):
            print(f"{scoretype} not enabled for {self.model_name}. skipping...")
            return
        timer = 'run_ftrchurn'
        sp.timer.start(timer)
        try:
            subprocess.check_call([
                'Rscript',
                self.paths.repo_ftrchurn,
                f"--librarypath={self.paths.repo_library}",
                f"--modelconfigfile={self.paths.model_config_file()}",
                f"--scoretype={scoretype}",
                f"--stridelength={stridelength}",
                f"--maxperiod={maxperiod}",
                ])
        except Exception as e:
            print('Rscript halted with non-zero exit status')
            raise SystemExit(e)
        sp.timer.stop(timer)

    # def run_mcore_train(self, sdate):
    #     sp.timer.start('run_mcore_train')
    #     try:
    #         subprocess.check_call([
    #             'Rscript', self.paths.repo_mcore, 
    #             f'--librarypath={self.paths.repo_library}',
    #             f'--defaultconfig={self.paths.default_config}',#.relative_to(pb.Path.home())}',
    #             f'--userconfig={self.paths.user_config}',#.relative_to(pb.Path.home())}',
    #             '--mode=train',
    #             # f'--input={self.paths.xy.relative_to(mcore_root)}/ModelData+ALL+{self.model_name}+{sdate}.feather',
    #             f'--input={self.paths.list_of_input(sdate)}',
    #             f'--model={self.paths.model}/{self.model_name}+{sdate}.rds',
    #             f'--output={self.paths.scores}/scores+{sdate}+{sdate}.csv',
    #             f'--eval={self.paths.eval}/{self.model_name}+{sdate}+{sdate}'
    #         ])
    #     except Exception as e:
    #         print('Rscript halted with non-zero exit status')
    #         raise SystemExit(e)

    #     sp.timer.stop('run_mcore_train')
    #     
    # def run_mcore_score(self, model_date, score_date):
    #     sp.timer.start('run_mcore_score')
    #     try:
    #         subprocess.check_call([
    #             'Rscript', self.paths.repo_mcore,
    #             f'--librarypath={self.paths.repo_library}',
    #             f'--defaultconfig={self.paths.default_config}',
    #             f'--userconfig={self.paths.user_config}',
    #             '--mode=score',
    #             f'--input={self.paths.list_of_input(score_date)}',
    #             f'--model={self.paths.read_latest_model()}',
    #             f'--output={self.paths.scores}/scores+{model_date}+{score_date}.csv',
    #             f'--eval={self.paths.eval}/{self.model_name}+{model_date}+{score_date}'
    #         ])
    #     except Exception as e:
    #         print('Rscript halted with non-zero exit status')
    #         raise SystemExit(e)
    #     sp.timer.stop('run_mcore_score')
