from tabulate import tabulate
from .. import store
def setup(force):
    '''
    run this command only once. It does the following:
        1. creates relevant MTA Tables: CLM_MTA_Response_Detailed
    '''

    SetupStatus.force = force

    detailed = SetupStatus()\
        .with_alias('detailed')\
        .with_name('CLM_MTA_Response_Detailed')\
        .with_exists(store.data.exists_mta_detailed())\
        .check(store.data.write_new_mta_detailed, if_exists='replace')

    header = ["Alias", "Name", "Status", "Action"]
    data = [
            detailed.lst,
            ]
    print('mta-log-table')
    print(tabulate(data, header, tablefmt="rounded_outline"))


class SetupStatus:
    force = None

    def with_name(self, name):
        self.name = name
        return self
    
    def with_status(self, status):
        self.status = status
        return self

    def with_action(self, action):
        self.action = action
        return self

    def with_exists(self, exists):
        self.exists = exists
        return self

    def with_alias(self, alias):
        self.alias = alias
        return self


    @property
    def lst(self):
        return [self.alias, self.name, self.status, self.action]

    def check(self, func, *args, **kwargs):
        if self.exists and not self.alias in self.force:
            self.status = "exists"
            self.action = "not replaced"
        elif self.exists and self.alias in self.force:
            func(*args, **kwargs)
            self.status = "exists"
            self.action = "replaced"
            self.with_status("exists").with_action("replaced")
        elif not self.exists:
            func(*args, **kwargs)
            self.status = "does not exist"
            self.action = "created"
        return self
