"""
sundog.store.data
"""
import pandas as pd
import datetime as dt

import smokypuppy as sp

from . import query

engine = sp.engine_from_env()

def exists_mta_detailed():
    return engine.con.has_table("CLM_MTA_Response_Detailed")

def write_new_mta_detailed(if_exists):
    columns = [
            'ID', 'YM', 'StartDate', 'EndDate', 'Name', 'CampaignName', 'CLMSegmentName', 'ChannelName', 'FirstTouch', 'LastTouch', 'LinearTouch', 'MultiTouch',
            'RemovalEffect', 'NormRemovalEffect', 'Outreach', 'TotalResponders', 'LTANum', 'MTANum', 'LTAPercent', 'MTAPercent',
            'AttributionWeight', 'AttributionPool',
               ]
    df = pd.DataFrame(columns=columns)
    engine.write_simple(df, "CLM_MTA_Response_Detailed", if_exists=if_exists)
    # dataobj.db.write_mc_dashboard(pd.DataFrame(columns=columns), if_exists=if_exists)



class Data:
    engine = engine

    def __init__(self, period_list):
        # self.engine = engine
        start, end = self.convert_period_to_dates(period_list)
        print("Reading Event_Execution_History Join Event_Attribution_History ...")
        self.eeh = self.engine.read(query.mta_input.render(start_date=start, end_date=end))
        print("Reading Event_Master ...")
        self.em = self.engine.read_stable(query.em.render())
        # self.rename_em()
        print("Preprocessing Data ...")
        self.pre_process()
        self.create_merged()

    # @property
    # def raw(self):
    #     return self.engine.read(query.mta_input.render())

    def convert_period_to_dates(self, period_list):
        if len(period_list) == 1:

            if ':' in period_list[0]:
                start, end = period_list[0].split(':')
                if start == 'start':
                    start = '200001'
                if end == 'end':
                    end = dt.date.today().strftime('%Y%m')

            elif period_list[0] == 'all':
                start = '200001'
                end = dt.date.today().strftime('%Y%m')

            elif period_list[0] == 'this':
                start = dt.date.today().strftime('%Y%m')
                end = dt.date.today().strftime('%Y%m')

            else:
                start = period_list[0]
                end = period_list[0]

            start, end = list(map(lambda x: pd.to_datetime(x, format='%Y%m'), [start, end])) 
            if not start.is_month_start:
                start = start - pd.tseries.offsets.MonthBegin()
            if not end.is_month_end:
                end = end + pd.tseries.offsets.MonthEnd()

            start, end = list(map(lambda x: x.strftime('%Y%m%d'), [start, end]))
            return start, end
        else:
            print('not supported yet')
            return '200001', dt.date.today().strftime('%Y%m')
            
                # ls_range = pd.date_range(start=start, end=end, freq='M')
                # ls_range.append(end)
                # ls_range = list(map(lambda x: x.strftime('%Y%m'), ls_range))
        
        
    
    def pre_process(self):
        self.eeh.Event_Execution_Date_ID = pd.to_datetime(self.eeh.Event_Execution_Date_ID, format="%Y%m%d")
        # TODO: make this happend in sql while reading the table
        self.eeh = self.eeh[self.eeh.Event_Execution_Date_ID.dt.date <= (dt.date.today() - dt.timedelta(weeks=1))]

    def time_groups(self, freq):
        return [(ind, group) for ind, group in self.eeh_em.groupby(pd.Grouper(key="Event_Execution_Date_ID", freq=freq))]

    # def one_month(self):  # last 30 days
    #     # self.eeh = self.engine.read(query.mta_input.render(start_date=start, end_date=end))
    #     newdata = self.__init__('this')
    #     return self.eeh_em[self.eeh_em.Event_Execution_Date_ID > (dt.datetime.today() - pd.DateOffset(months=1))]

    def create_merged(self):
        df = self.eeh.merge(self.em, left_on='Event_ID', right_on='ID')
        df.loc[df.sale_net_val.notnull(), 'Event_ID'] = df[df.sale_net_val.notnull()].Event_ID.astype(str) + '(' + df[df.sale_net_val.notnull()].sale_net_val.astype(str) + ')'
        df = df.sort_values('Event_Execution_Date_ID')
        self.eeh_em = df
