"""
srcpy.ops
"""

from . import ffi
from . import data
from .. import tools 


import sys
import click
import pandas as pd
import smokypuppy as sp

dataobj = data.Data()

class Ops:

    def train(self, dateT):
        dateT = tools.dates.from_st(dateT)
        click.secho(f'Starting MFactory Train with OBSENDDATE={dateT.st}')
        for _, row in dataobj.read_and_process_mc_master().iterrows():
            if not row.Enabled:
                click.secho(f"{row.ModelName} is not enabled. Skipping and moving to next model", fg="yellow")
                continue
            if not row.TrainingIsDue:
                click.secho(f"{row.ModelName} Training is NOT Due Today. Skipping and moving to next model...", fg="yellow")
                continue
            click.secho(f"{row.ModelName} Training is Due Today", fg="yellow")
            click.secho(f"{row.ModelName} is enabled. Proceeding...")
            timerlabel = 'mfactory_train_onemodel'
            sp.timer.start(timerlabel)
            modeldata = data.Model(row.ModelName)
            modeldata.update_mc_master_status_train("started")

            dateT_n = dateT.sub(row.ResponseWindowDays)
            dateT_2n = dateT.sub(row.ResponseWindowDays*2)

            paths = tools.Paths(row.ModelName)
            paths.make_new_empty()
            rffi = ffi.Rinterface(row.ModelName)

            try:
                rffi.run_featurizer(dateT_2n.st)
                rffi.run_attachresponseandfilter(startdate=dateT_2n.next().st, enddate=dateT_n.st, mode='train')
                rffi.run_mcore('train')
                gain_oos = paths.create_gain(dateT_n.st) # training date

                rffi.run_featurizer(dateT_n.st)
                rffi.run_attachresponseandfilter(startdate=dateT_n.next().st, enddate=dateT.st, mode='train')
                rffi.run_mcore('score')
                gain_oot = paths.create_gain(dateT_n.st)
                rffi.run_mcore('train') # train

                model_gain_df = paths.create_final_gain([gain_oos, gain_oot])
                # model_gain_df = paths.create_gain_chart(dateT_2n.st, dateT_n.st)
                modeldata.update_mc_dashboard(model_gain_df)
                modeldata.postflight_check_train()
            except Exception as e:
                print(e)
                print("execution halted")
                modeldata.update_mc_master_status_train("halted")
                print("updated halted status to db")
                sys.exit()
            elapsed = sp.timer.stop(timerlabel)
            modeldata.update_mc_master_status_train("completed", elapsed.in_words())
            modeldata.update_mc_master_train()

        sp.timer.print_all_times()

    def score(self, dateT):
        dateT = tools.dates.from_st(dateT)
        click.secho(f'Starting MFactory Train with OBSENDDATE={dateT.st}')

        for _, row in dataobj.read_and_process_mc_master().iterrows():
            if not row.Enabled:
                click.secho(f"{row.ModelName} is not enabled. Skipping and moving to next model...", fg="yellow")
                continue
            if not row.ScoringIsDue:
                click.secho(f"{row.ModelName} Scoring is NOT Due Today. Skipping and moving to next model...", fg="yellow")
                continue
            timerlabel = 'mfactory_score_onemodel'
            sp.timer.start(timerlabel)
            modeldata = data.Model(row.ModelName)
            modeldata.update_mc_master_status_score("started")
            click.secho(f"{row.ModelName} Scoring is Due Today", fg="yellow")
            click.secho(f"{row.ModelName} is enabled. Proceeding...")
            dateT_n = dateT.sub(row.ResponseWindowDays)

            rffi = ffi.Rinterface(row.ModelName)
            paths = tools.Paths(row.ModelName)

            try:
                rffi.run_featurizer(dateT.st)
                rffi.run_attachresponseandfilter(startdate=dateT.next().st, enddate=dateT.add(row.ResponseWindowDays).st, mode='score')
                rffi.run_mcore("score")
                rffi.run_ftrchurn(scoretype='FTR', stridelength=row.ResponseWindowDays, maxperiod=row.FTRMaxWindow)
                rffi.run_ftrchurn(scoretype='Churn', stridelength=row.ResponseWindowDays, maxperiod=row.ChurnMaxWindow)
                modeldata.write_scores_to_db()
                modeldata.postflight_check_score()
            except Exception as e:
                print(e)
                print("execution halted")
                modeldata.update_mc_master_status_score("halted")
                print("updated halted status to db")
                sys.exit()
            elapsed = sp.timer.stop(timerlabel)
            modeldata.update_mc_master_status_score("completed", elapsed.in_words())
            modeldata.update_mc_master_score()

        sp.timer.print_all_times()

    def write_new_mc_config(self, if_exists):
        default_config = pd.read_csv(tools.Paths.data_default_config)
        dataobj.db.write_mc_config(default_config, if_exists=if_exists)

    def write_new_mc_check(self, if_exists):
        # default_check = pd.read_csv(tools.Paths.default_check)
        columns = ['Type', 'Status', 'Check', 'Expected', 'Actual']
        df_lst= []
        dataobj.db.stats.read_stats()
        for cls in data.check.Check.__subclasses__():
            obj = cls()
            if obj.type == 'pre':
                obj.run_check()
            df_lst.append(obj.lst)
        # bh = data.check.BillHeader().run_check()
        # count_ftr = data.check.CountFTR()
        # count_churn = data.check.CountChurn()
        # df_lst = [
        #         bh.lst,
        #         count_ftr.lst,
        #         count_churn.lst,
        #         ]
        new_check = pd.DataFrame(df_lst, columns=columns)
        dataobj.db.write_mc_check(new_check, if_exists=if_exists)

    def write_new_mc_master(self, if_exists):
        default_master = pd.read_csv(tools.Paths.data_default_master)
        default_master.loc[default_master.ModelName.eq("NextTrxn15"), "FTRMaxWindow"] = dataobj.db.read_active_period()
        default_master.loc[default_master.ModelName.eq("NextTrxn30"), "ChurnMaxWindow"] = dataobj.db.read_active_period()/2
        dataobj.db.write_mc_master(default_master, if_exists=if_exists)

    def write_new_mc_percent(self, if_exists):
        mc_master = dataobj.db.read_mc_master()
        columns = []
        for model_name in mc_master.ModelName:
            columns.append(f'{model_name}_All')
        for model_name in mc_master.ModelName:
            columns.append(f'{model_name}_CLMSegment')
        columns.extend([
            'FTR_All',
            'Churn_All',
            'FTR_CLMSegment',
            'Churn_CLMSegment',
            ])

        df = dataobj.db.read_cust_with_seg()
        df[columns]= ""
        dataobj.db.write_mc_percent(df, if_exists=if_exists)

    def write_new_mc_dashboard(self, if_exists):
        columns = [
                'Decile', 'Conversions', 'ConversionsPercent', 'Gain', 'Quantile', 'Cutoff', 'Obs.1', 'Obs.0',
                'Pred.1', 'Pred.0', 'CumlConversions', 'Precision', 'F1', 'TrainingEndDate', 'TestType', 'ChartType',
                'Base', 'CumlBase', 'CumlConversionsPercent', 'ModelName',
                   ]
        dataobj.db.write_mc_dashboard(pd.DataFrame(columns=columns), if_exists=if_exists)

