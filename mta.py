"""
sundog.mta
"""

import os
import re
import datetime as dt
import pandas as pd
from functional import seq
import ChannelAttribution as chattr

from . import store

store.data


# class MTA:

#     def __init__(self, data):
#         self.data = data

def create_uniq_cust_jrn(df):
    uniq_cust_jrn = (
        seq(df[['Customer_ID', 'Event_ID']])
        .group_by_key()  # group event_ids according to customer_ids, size = unique customer count
        .map(lambda x: x[1])  # create a seq: list of list with consecutive events as columns, size = unique customer count
        .map(lambda x: ' > '.join(map(lambda y: str(y), x)))  # join the list of events for each customer with a '>', size = unique customer count
        .map(lambda x: re.split(r'(.*?\(.*?\))', x))  # split at existence of revenue (meaning: txn occured), changed from flat_map to map. size = unique customer count
        .map(lambda x: ''.join(x))
        .map(lambda x: re.sub(r'\(.*?\)', ' > txn', x))  # replace revenue with 'txn', size = unique customer count
        # .list()
        # .to_pandas(columns=None)
    )
    return uniq_cust_jrn

def create_chattr_input(uniq_cust_jrn):

    txn_end = (
        uniq_cust_jrn
        .filter(lambda x: re.findall('txn', x))
        .flat_map(lambda x: re.split('(?<=txn).', x))  # size = 895 > total number of txns
        .filter(lambda x: x.endswith('txn'))  # size = 791 = total number of txns
        # .map(lambda x: re.sub('(?<=txn).*(?!txn)', '', x))
    )
    cust_trips = pd.Series(uniq_cust_jrn).str.extractall('(\d.*?txn|\d.*\d)')
    cust_trips[0].values

    ans_trip_df = (
        pd.Series(cust_trips[0].values)
        .value_counts()
        .to_frame()
        .reset_index()
        .rename(columns={'index': 'trip', 0: 'trip_count'})
    )

    ans_trip_df['len'] = (
        ans_trip_df
        .trip
        .str
        .extractall('(\d*)')[0]
        .dropna()
        .groupby(level=[0])
        .size()
    )
    ans_trip_df['num_txn'] = ans_trip_df.trip.str.count('txn')
    ans_trip_df['end_txn'] = ans_trip_df.trip.str.endswith('txn').astype(int)
    txn_df = ans_trip_df[ans_trip_df.trip.str.endswith('txn')].rename(columns={'trip_count': 'txn_count'})[['trip', 'txn_count','len']]
    txn_df['trip'] = txn_df.trip.str.replace(' > txn', '')

    ans_convert_df = (
        ans_trip_df[~ans_trip_df.trip.str.endswith('txn')]
        .rename(columns={'trip_count': 'nulls'})[['trip', 'nulls','len']]
        .merge(right=txn_df, how='outer', on=['trip', 'len'])
        )
    chattr_input = ans_convert_df.rename(columns={"trip": "path", "nulls": "total_null", "txn_count": "total_conversions"})
    chattr_input.total_conversions = chattr_input.total_conversions.fillna(0)
    chattr_input.total_null = chattr_input.total_null.fillna(0)
    chattr_input = chattr_input.drop(["len"], axis=1)
    return chattr_input

def create_result_detailed(chattr_input, df, data, *args, **kwargs): # this is for the CLM_MTA_Response_Detailed, creating a new function to refactor the old
    if os.getenv("MTA_ORDER", "2") == "CALCULATE":
        markov_order = chattr.choose_order(chattr_input, var_path='path', var_conv='total_conversions', var_null='total_null')[2]
        print(f"calculated optimized order: {markov_order}")
    else:
        markov_order = int(os.getenv("MTA_ORDER", "2"))
    heur_df = chattr.heuristic_models(chattr_input, var_path='path', var_conv='total_conversions')
    markov_ls = chattr.markov_model(chattr_input, var_path='path', var_conv='total_conversions',
                                    var_null='total_null', order=markov_order, out_more=True, *args, **kwargs)
    chattr_merged = heur_df.merge(markov_ls['result'], on='channel_name').merge(markov_ls['removal_effects'], on='channel_name')
    chattr_merged = chattr_merged.convert_dtypes().apply(pd.to_numeric, errors='ignore')
    chattr_marketing = chattr_merged.merge(data.em, how='inner', left_on='channel_name', right_on='ID')
    chattr_marketing['StartDate'] = df.Event_Execution_Date_ID.min().strftime('%Y%m%d')
    chattr_marketing['EndDate'] = df.Event_Execution_Date_ID.max().strftime('%Y%m%d')
    chattr_marketing['YM'] = chattr_marketing.EndDate.str[:-2]
    chattr_marketing = chattr_marketing.rename(columns={
        'first_touch': 'FirstTouch',
        'last_touch': 'LastTouch',
        'linear_touch': 'LinearTouch',
        'total_conversions': 'MultiTouch',
        'removal_effect': 'RemovalEffect',
        })
    chattr_marketing['NormRemovalEffect'] = chattr_marketing.RemovalEffect / chattr_marketing.RemovalEffect.sum()
    chattr_marketing = chattr_marketing[[
        'ID', 'YM', 'StartDate', 'EndDate', 'Name', 'CampaignName', 'CLMSegmentName', 'ChannelName', 'FirstTouch', 'LastTouch',
        'LinearTouch', 'MultiTouch', 'RemovalEffect', 'NormRemovalEffect'
        ]]
    # start dates, end dates
    # start_date = df.Event_Execution_Date_ID.min().strftime("%Y%m%d")
    # end_date = df.Event_Execution_Date_ID.max().strftime("%Y%m%d")
    # chattr_marketing['StartDate'] = start_date
    # chattr_marketing['EndDate'] = end_date
    # chattr_marketing['YM'] = chattr_marketing.EndDate.str[:-2]
    outreach_df = df.ID.value_counts().rename('Outreach').reset_index().rename(columns={'index': 'ID'})
    chattr_marketing = chattr_marketing.merge(outreach_df, how='left', on='ID')
    chattr_marketing['TotalResponders'] = chattr_marketing.LastTouch.sum()
    chattr_marketing['LTANum'] = chattr_marketing.LastTouch
    chattr_marketing['MTANum'] = chattr_marketing.MultiTouch
    chattr_marketing['LTAPercent'] = chattr_marketing['LTANum'] / chattr_marketing.Outreach * 100
    chattr_marketing['MTAPercent'] = chattr_marketing['MTANum'] / chattr_marketing.Outreach * 100
    chattr_marketing['AttributionWeight'] = chattr_marketing.NormRemovalEffect
    chattr_marketing['AttributionPool'] = chattr_marketing.TotalResponders
    return chattr_marketing

def create_melted_table(chattr_marketing, df):
    cm = chattr_marketing
    # df = df.rename(columns={'Campaign': 'CampaignName', 'Theme': 'CLMSegmentName', 'Channel': 'ChannelName'})
    df_wide = pd.DataFrame({
        'Outreach': df.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName'])['Customer_ID'].count(),
        'First_Touch': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().FirstTouch,
        'Last_Touch': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().LastTouch,
        'Linear_Touch': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().LinearTouch,
        'Multi_Touch': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().MultiTouch,
        'RemovalEffect': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().RemovalEffect,
        'NormRemovalEffect': cm.groupby(['CampaignName', 'CLMSegmentName', 'ChannelName']).sum().NormRemovalEffect,
    }).reset_index()
    
    df_long = pd.melt(
            df_wide, id_vars=['CampaignName', 'CLMSegmentName', 'ChannelName', 'Outreach',
                'RemovalEffect', 'NormRemovalEffect'],
            var_name='AttributionMechanism', value_name='AttributedResponse')
    df_long['ResponseRate'] = df_long['AttributedResponse'] / df_long['Outreach'] * 100
    start_date = df.Event_Execution_Date_ID.min().strftime("%Y%m%d")
    end_date = df.Event_Execution_Date_ID.max().strftime("%Y%m%d")
    df_long['StartDate'] = start_date
    df_long['EndDate'] = end_date
    df_long['YM'] = df_long.EndDate.str[:-2]
    # -- below statements are for backward compatibility of mta ui
    df_long['Theme'] = df_long['CampaignName']
    df_long['Segment'] = df_long['CLMSegmentName']
    df_long['Channel'] = df_long['ChannelName']
    print(f"Processing complete for date range {start_date} to {end_date}")
    return df_long

def one_month():  # last 30 days
    previous_month = (dt.datetime.today() - pd.DateOffset(months=1)).strftime("%Y%m")
    sincelastmonth = store.Data([f"{previous_month}:end"])
    return sincelastmonth.eeh_em[sincelastmonth.eeh_em.Event_Execution_Date_ID > ((dt.datetime.today() - dt.timedelta(days=7)) - pd.DateOffset(months=1))]

def create_long_table_for_each_period(data, *args, **kwargs):
    CLM_MTA_Response_Detailed_ls = []
    CLM_MTA_Response_ls = []
    print("Processing data for each month...")
    for ind, df in data.time_groups('M'):
        if (ind.year == (dt.date.today() - dt.timedelta(days=7)).year) and (ind.month == (dt.date.today() - dt.timedelta(days=7)).month):
            # possible values: SLIDING, STATIC
            if os.getenv("MTA_CURRENT_PERIOD", "SLIDING") == "SLIDING":
                df = one_month() 
                print('calculating current months data using last 30 days of data')
        df_uniq_cust_jrn = create_uniq_cust_jrn(df)
        print("Journey for each customer", df_uniq_cust_jrn.size())
        if df_uniq_cust_jrn.size() == 0:
            continue

        df_chattr_input = create_chattr_input(df_uniq_cust_jrn)
        if df_chattr_input.total_conversions.sum() == 0:
            continue
        CLM_MTA_Response_Detailed_Single_Period = create_result_detailed(df_chattr_input, df, data, *args, **kwargs)
        CLM_MTA_Response_Single_Period = create_melted_table(CLM_MTA_Response_Detailed_Single_Period, df)
        CLM_MTA_Response_Detailed_ls.append(CLM_MTA_Response_Detailed_Single_Period)
        CLM_MTA_Response_ls.append(CLM_MTA_Response_Single_Period)
    
    if len(CLM_MTA_Response_ls) == 0:
        print("Data not suitable for MTA")
        # sys.exit(1)
        raise Exception('Data not suitable')

    CLM_MTA_Response_Detailed = pd.concat(CLM_MTA_Response_Detailed_ls)
    CLM_MTA_Response = pd.concat(CLM_MTA_Response_ls)
    return CLM_MTA_Response_Detailed, CLM_MTA_Response

