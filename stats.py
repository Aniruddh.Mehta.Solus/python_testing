import smokypuppy as sp

class Stats:
    engine = sp.engine_from_env()

    def read_stats(self):
        self.cdmbh_count = self.read_cdmbh_count()
        self.cdmbd_count = self.read_cdmbd_count()
        self.cdmbd_date_min = self.read_cdmbd_date_min()
        self.cdmbd_date_max = self.read_cdmbd_date_max()
        self.cone_count = self.read_cone_count()
        self.mc_percent_count = self.read_mc_percent_count()
        self.mc_master_count = self.read_mc_master_count()
        self.mc_dashboard_count = self.read_mc_dashboard_count()
        self.mc_master_col_count = self.read_mc_master_col_count()
        self.mc_dashboard_col_count = self.read_mc_dashboard_col_count()
        self.mc_percent_col_count = self.read_mc_percent_col_count()

    def _query_one_int(self, query):
        return int(self._query_one(query))

    def _query_one_str(self, query):
        return str(self._query_one(query))

    def _query_one(self, query):
        return self.engine.read(query).values[0][0]

    def read_mc_percent_col_count(self):
        return self._query_one_int("select count(column_name) from information_schema.columns where table_name = 'CLM_MCore_Customer_Percentile' ")

    def read_mc_dashboard_col_count(self):
        return self._query_one_int("select count(column_name) from information_schema.columns where table_name = 'Dashboard_Insights_Propensity' ")

    def read_mc_master_col_count(self):
        return self._query_one_int("select count(column_name) from information_schema.columns where table_name = 'CLM_MCore_Model_Master' ")

    def read_mc_master_count(self):
        return self._query_one_int('select count(1) from CLM_MCore_Model_Master')
    
    def read_mc_dashboard_count(self):
        return self._query_one_int('select count(1) from Dashboard_Insights_Propensity')

    def read_mc_percent_count(self):
        return self._query_one_int('select count(Customer_Id) from CLM_MCore_Customer_Percentile')

    def read_cone_count(self):
        return self._query_one_int('select count(Customer_Id) from V_CLM_Customer_One_View')

    def read_cdmbh_count(self):
        return self._query_one_int('select count(1) from CDM_Bill_Header')

    def read_cdmbd_count(self):
        return self._query_one_int('select count(1) from CDM_Bill_Details')

    def read_cdmbd_date_min(self):
        return self._query_one_str("select min(Bill_Date) from CDM_Bill_Details")

    def read_cdmbd_date_max(self):
        return self._query_one_str("select max(Bill_Date) from CDM_Bill_Details")
