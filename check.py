from . import db
from abc import ABC
import pendulum as pm
import smokypuppy as sp


class Check(ABC):
    engine = sp.engine_from_env()
    db = db.DB()

    def __init__(self):
        self.status = 'PENDING'
        self.type = None
        self.name = None
        self.expected = None
        self.actual = ''

    def check_actual(self):
        pass

    def query_one(self, query=None):
        return self.engine.read(query).values[0][0]

    def query_one_int(self, query=None):
        return int(self.engine.read(query).values[0][0])

    def run_check(self):
        self.actual = self.check_actual()
        self.status = self.check_status()
        return self

    def check_status(self):
        if type(self.expected) == str:
            if self.expected.startswith('>'):
                if self.actual > int(self.expected[1:]):
                    return 'OK'
                else: return 'FAIL'
            elif self.expected.startswith('<'):
                if self.actual < int(self.expected[1:]):
                    return 'OK'
                else: return 'FAIL'
        else:
            if self.actual == self.expected:
                return 'OK'
            elif abs(self.actual - self.expected) < self.db.read_check_fail_margin():
                return 'WARN'
            else:
                return 'FAIL'

    @property
    def lst(self):
        return [self.type, self.status, self.name, self.expected, self.actual]

    def update_db(self):
        old = self.db.read_mc_check()
        mask = old.Check.eq(self.name)
        old.loc[mask, "Status"] = self.status
        old.loc[mask, "Actual"] = self.actual
        new = old
        self.db.write_mc_check(new)

    # def create_tests(self):

    #     simple_check_list = [
    #     ['pre', 'fill.billheader', '>0', 'select count(1) from CDM_Bill_Header'],
    #     ['pre', 'fill.billdetails', '>0', 'select count(1) from CDM_Bill_Details'],
    #     ['post_train', 'fill.dashboard', '>0', 'select count(1) from Dashboard_Insights_Propensity'],
    #     ['post_score', 'fill.scores', '>0', 'select count(1) from CLM_MCore_Customer_Percentile'],
    #     ]

    #     for item in simple_check_list:
    #         type(f'{item[1]}', (Check,), {
    #             'type': item[0],
    #             'status': 'PENDING',
    #             'name': item[1],
    #             'expected': item[2],
    #             'actual': self.query_one(query=item[3])
    #             })


class FillBillHeader(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'fill.billheader'
        self.expected = '>0'

    def check_actual(self):
        return self.db.stats.cdmbh_count


class FillBillDetails(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'fill.billdetails'
        self.expected = '>0'

    def check_actual(self):
        return self.db.stats.cdmbd_count

class FillModelMaster(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'fill.modelmaster'
        self.expected = '>0'

    def check_actual(self):
        return self.db.stats.mc_master_count


class CountPercentile(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'count.percentile'
        self.expected = self.db.stats.cone_count

    def check_actual(self):
        return self.db.stats.mc_percent_count

class ColsModelMaster(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'cols.modelmaster'
        self.expected = 17

    def check_actual(self):
        return self.db.stats.mc_master_col_count

class ColsDashboard(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'cols.dashboard'
        self.expected = 20

    def check_actual(self):
        return self.db.stats.mc_dashboard_col_count

class ColsPercentile(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'cols.percentile'
        self.expected = 12

    def check_actual(self):
        return self.db.stats.mc_percent_col_count

class MinBillDetails(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'min.billdetails'
        self.expected = '>20000101'

    def check_actual(self):
        strdate =  self.db.stats.cdmbd_date_min
        return int(''.join(strdate.split('-')))

class MaxBillDetails(Check):

    def __init__(self):
        super().__init__()
        self.type = 'pre'
        self.name = 'max.billdetails'
        self.expected = '<' + pm.today().format('YYYYMMDD')

    def check_actual(self):
        strdate =  self.db.stats.cdmbd_date_max
        return int(''.join(strdate.split('-')))


class CountDashboard(Check):

    def __init__(self):
        super().__init__()
        self.type = 'post_train'
        self.name = 'count.dashboard'
        self.expected = 60

    def check_actual(self):
        return self.db.stats.mc_dashboard_count

class CountFTR(Check):

    def __init__(self):
        super().__init__()
        self.type = 'post_score'
        self.name = 'count.scores.ftr'
        self.expected = self.db.read_ftr_actual_count()

    def check_actual(self):
        return self.query_one_int('select count(FTR_All) from CLM_MCore_Customer_Percentile')


class CountChurn(Check):

    def __init__(self):
        super().__init__()
        self.type = 'post_score'
        self.name = 'count.scores.churn'
        self.expected = self.db.read_churn_actual_count()

    def check_actual(self):
        return self.query_one_int('select count(Churn_All) from CLM_MCore_Customer_Percentile')


class FillPercentile(Check):

    def __init__(self):
        super().__init__()
        self.type = 'post_score'
        self.name = 'fill.percentile'
        self.expected = '>0'

    def check_actual(self):
        return self.query_one_int('select count(1) from CLM_MCore_Customer_Percentile')

